﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour

{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Get the input in the form of -1 to 1 and store
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Ask our current game object (that this script is attached to) for
        // any Rigidbody component also attached to the same object
        Rigidbody playerPhysics = GetComponent<Rigidbody>();

        //Set up the new movement vector
        Vector3 movement = Vector3.zero;
        movement.x = horizontal;
        movement.z = vertical;

        // Assign the vector to the player's velocity
        playerPhysics.velocity = movement;

    }
}
